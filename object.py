#!/usr/bin/env python3


from utilities import indent


class Constant:

    def __init__(self, name, ctype, value=None):
        self.name = name
        self.ctype = ctype
        self.value = value

    def to_string(self, n=0):
        value = f" := {self.value}" if self.value is not None else ""
        return (f"{indent(n)}constant {self.name} : {self.ctype}{value};")


class Signal:

    def __init__(self, name, stype, value=None):
        self.name = name
        self.stype = stype
        self.value = value

    def to_string(self, n=0):
        value = f" := {self.value}" if self.value is not None else ""
        return (f"{indent(n)}signal {self.name} : {self.stype}{value};")


class Variable:

    def __init__(self, name, vtype, value=None):
        self.name = name
        self.vtype = vtype
        self.value = value

    def to_string(self, n=0):
        value = f" := {self.value}" if self.value is not None else ""
        return (f"{indent(n)}signal {self.name} : {self.vtype}{value};")
