# VHDLint

A linter for the VHDL programming language.

## Possible VHDL Statements

### Done

- library \<name\>;
- use \<library-package-all\>;
- constant \<name\> : \<type\>;
- constant \<name\> : \<type\> := \<value\>;
- signal \<name\> : \<type\>;
- signal \<name\> : \<type\> := \<value\>;
- variable \<name\> : \<type\>;
- variable \<name\> : \<type\> := \<value\>;
- entity \<name\> is
  - \<port-name\> : \<direction\> \<type\>;
  - end entity \<name\>;
- component \<name\> is
  - \<port-name\> : \<direction\> \<type\>;
  - end component \<name\>;
- subtype \<name\> is \<type\>;
- type \<name\> is (\<enums\>);
- type \<name\> is record
  - \<name\> : \<resolved-type\>;
  - end record \<name\>;

## Not Done

- architecture \<name\> of \<entity-name\> is
  - begin
  - end architecture \<name-or-nothing\>;
- \<impure-or-nothing\> function \<name\>(\<arg-names\> : \<arg-types\>) \
    return \<type\>; -- only necessary in package files
- \<impure-or-nothing\> function \<name\>(\<arg-names\> : \<arg-types\>)
  - return \<type\> is
  - begin
  - end function \<name-or-nothing\>;
  - end \<name\>;
- procedure \<name\>(\<arg-names\> : \<arg-types\>);
- procedure \<name\>(\<arg-names\> : \<arg-types\>) is
  - begin
  - end procedure \<name-or-nothing\>;
  - end \<name\>;
- \<process-name-or-nothing\> : process(\<sensitivity-list\>) \<is-or-nothing\>
  - begin
  - end process \<process-name-or-nothing\>;
- \<gen-name-or-nothing\> : for \<index\> in \<left\> (down)to \<right\> generate
  - begin
  - end generate \<gen-name-or-nothing\>;
- \<gen-name-or-nothing\> : if \<filter\> generate
  - begin
  - end generate \<gen-name-or-nothing\>;
- instance TODO fill this
