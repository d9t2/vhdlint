#!/usr/bin/env python3


def indent(n):
    return " " * n
