#!/usr/bin/env python3


from utilities import indent


class Component:

    class Generic:

        def __init__(self, name, ptype, value=None, last=False):
            self.name = name
            self.ptype = ptype
            self.value = value
            self.last = last

        def to_string(self, n=0):
            last = "" if self.last else ";"
            value = f" := {self.value}" if self.value is not None else ""
            return f"{indent(n)}{self.name} : {self.ptype}{value}{last}"

    class Generics:

        def __init__(self, generics):
            if isinstance(generics, list) and len(generics) > 0:
                self.generics = []
                for generic in generics:
                    if isinstance(generic, tuple):
                        if len(generic) == 2:
                            self.generics.append(Component.Generic(generic[0],
                                                                   generic[1]))
                        else:
                            self.generics.append(Component.Generic(generic[0],
                                                                   generic[1],
                                                                   generic[2]))
                    else:
                        self.generics.append(generic)
                self.generics[-1].last = True
            else:
                raise ValueError("Argument generics must be a list.")

        def __len__(self):
            return len(self.generics)

        def to_string(self, n=0):
            ret = ""
            for generic in self.generics:
                ret += generic.to_string(n) + "\n"
            return ret

    class Port:

        def __init__(self, name, direction, ptype, last=False):
            self.name = name
            self.direction = direction
            self.ptype = ptype
            self.last = last

        def to_string(self, n=0):
            last = "" if self.last else ";"
            return (f"{indent(n)}{self.name} : "
                    f"{self.direction} {self.ptype}{last}")

    class Ports:

        def __init__(self, ports):
            if isinstance(ports, list) and len(ports) > 0:
                self.ports = []
                for port in ports:
                    if isinstance(port, tuple):
                        self.ports.append(Component.Port(port[0],
                                                         port[1],
                                                         port[2]))
                    else:
                        self.ports.append(port)
                self.ports[-1].last = True
            else:
                raise ValueError("Argument ports must be a list.")

        def __len__(self):
            return len(self.ports)

        def to_string(self, n=0):
            ret = ""
            for port in self.ports:
                ret += port.to_string(n) + "\n"
            return ret

    def __init__(self, name, generics, ports):
        self.name = name
        if isinstance(generics, list):
            self.generics = Component.Generics(generics)
        else:
            self.generics = generics
        if isinstance(ports, list):
            self.ports = Component.Ports(ports)
        else:
            self.ports = ports

    def to_string(self, n=0):
        ret = (f"{indent(n)}component {self.name} is\n"
               + self.format_generics(n + 2)
               + self.format_ports(n + 2) +
               f"{indent(n)}end component {self.name};")
        return ret

    def format_generics(self, n=0):
        if len(self.generics) != 0:
            return (f"{indent(n)}generic (\n"
                    + self.generics.to_string(n=n + 2) +
                    f"{indent(n)});\n")
        else:
            return ""

    def format_ports(self, n=0):
        return (f"{indent(n)}port (\n"
                + self.ports.to_string(n=n + 2) +
                f"{indent(n)});\n")
