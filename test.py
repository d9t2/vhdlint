#!/usr/bin/env python3


import sys


from component import Component
from entity import Entity
from type import Subtype, Type


def main(args):
    entity = Entity("tie_off",
                    [("C_DATA_WIDTH", "natural", "32"),
                     ("C_USER_WIDTH", "natural")],
                    [("clk", "in", "std_logic"),
                     ("reset", "in", "std_logic"),
                     ("enable", "in", "std_logic")])
    print(entity.to_string(n=0))
    component = Component("tie_off",
                          [("C_DATA_WIDTH", "natural", "32"),
                           ("C_USER_WIDTH", "natural")],
                          [("clk", "in", "std_logic"),
                           ("reset", "in", "std_logic"),
                           ("enable", "in", "std_logic")])
    print(component.to_string(n=0))
    subtype = Subtype("ushort_t", "std_logic_vector(16 - 1 downto 0)")
    print(subtype.to_string(n=0))
    type_enum = Type.Enum("opcode_t", ["VRT_e", "ACK_e", "TOD_e"])
    print(type_enum.to_string(n=0))
    type_record = Type.Record("in_out_t", [("take", "std_logic")])
    print(type_record.to_string(n=0))


if __name__ == "__main__":
    main(sys.argv)
