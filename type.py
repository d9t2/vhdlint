#!/usr/bin/env python3


from utilities import indent


class Subtype:

    def __init__(self, name, stype):
        self.name = name
        self.stype = stype

    def to_string(self, n=0):
        return f"{indent(n)}subtype {self.name} is {self.stype};"


class Type:

    class Enum:

        def __init__(self, name, enums):
            self.name = name
            self.enums = enums

        def to_string(self, n=0):
            enums = ""
            for i, enum in enumerate(self.enums):
                if i < len(self.enums) - 1:
                    enums += f"{enum}, "
                else:
                    enums += enum
            return f"{indent(n)}type {self.name} is ({enums});"

    class Record:

        class Element:

            def __init__(self, name, retype):
                self.name = name
                self.retype = retype

            def to_string(self, n=0):
                return (f"{indent(n)}{self.name} : {self.retype};")

        class Elements:

            def __init__(self, elements):
                self.elements = []
                for element in elements:
                    if isinstance(element, tuple):
                        self.elements.append(Type.Record.Element(element[0],
                                                                 element[1]))
                    else:
                        self.elements.append(element)

            def to_string(self, n=0):
                ret = ""
                for element in self.elements:
                    ret += element.to_string(n) + "\n"
                return ret

        def __init__(self, name, elements):
            self.name = name
            if isinstance(elements, list):
                self.elements = Type.Record.Elements(elements)
            else:
                self.elements = elements

        def to_string(self, n=0):
            return (f"{indent(n)}type {self.name} is record\n"
                    + self.elements.to_string(n=n+2) +
                    f"end record {self.name};\n")
