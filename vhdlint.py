#!/usr/bin/env python3


from abc import ABC, abstractmethod
import sys


# Generic Utility Functions
def indent_length(line):
    for index in range(len(line)):
        if line[index] != " ":
            return index


# Generic Linting
SPACES_PER_INDENT = 2


class LintResult:

    def __init__(self, success, id=None, message=None, line_index=None,
                 char_index=None):
        self.success = success
        self.id = id
        self.message = message
        self.line_index = line_index
        self.char_index = char_index

    def __str__(self):
        if self.success:
            return "LintSuccess"
        else:
            return (f"{self.id}: {self.message}: {self.char_index}: "
                    f"{self.line_index} "
                    "[vhdlint]")


LINT_SUCCESS = LintResult(True)


class LineLinter(ABC):

    def __init__(self, id, message):
        self.id = id
        self.message = message

    @abstractmethod
    def lint(self, line, line_index, **kwargs):
        pass


class MultilineLinter(ABC):

    def __init__(self, id, message, single_line_linters=[]):
        self.id = id
        self.message = message
        self.single_line_linters = single_line_linters

    def sub_lint(self, line, line_index, **kwargs):
        for single_line_linter in single_line_linters:
            ret = single_line_linter(line, line_index, **kwargs)
            if ret != LINT_SUCCESS:
                return ret
        return LINT_SUCCESS

    @abstractmethod
    def lint(self, lines, line_index, **kwargs):
        pass


# Linters
class G001TabCharacter(LineLinter):

    def __init__(self):
        super().__init__("G001", "Tab character")

    def lint(self, line, line_index, **kwargs):
        try:
            index = line.index("\t")
        except ValueError:
            return LINT_SUCCESS
        return LintResult(False, self.id, self.message,
                          line_index, char_index=index)


class G002TrailingWhitespace(LineLinter):

    def __init__(self):
        super().__init__("G002", "Trailing whitespace")

    def lint(self, line, line_index, **kwargs):
        if line[-1] == " ":
            return LintResult(False, self.id, self.message,
                              line_index, char_index=len(line) - 1)
        return LINT_SUCCESS


class G003WhitespaceBeforeSemiColon(LineLinter):

    def __init__(self):
        super().__init__("G003", "Whitespace before semi-colon")

    def lint(self, line, line_index, **kwargs):
        index = len(line) - line[::-1].index(";") - 1
        if line[index - 1] == " ":
            return LintResult(False, self.id, self.message,
                              line_index, char_index=index - 1)
        return LINT_SUCCESS


class G004IncorrectIndentation(LineLinter):

    def __init__(self):
        super().__init__("G004", "Incorrect indentation level")

    def lint(self, line, line_index, **kwargs):
        expected_indent_level = kwargs.get("expected_indent_level")
        indent_len = indent_length(line)
        if indent_len != SPACES_PER_INDENT * expected_indent_level:
            return LintResult(False, self.id, self.message,
                              line_index, char_index=indent_len)
        return LINT_SUCCESS


class S001SignalAssignmentSpaces(LineLinter):

    def __init__(self):
        super().__init__("S001", "Signal assignments should have a single "
                                 "space on either side")

    def lint(self, line, line_index, **kwargs):
        index = line.index("<=")
        if line[index - 1] != " ":
            return LintResult(False, self.id, self.message,
                              line_index, char_index=index - 1)
        if line[index + 2] != " ":
            return LintResult(False, self.id, self.message,
                              line_index, char_index=index + 1)
        if line[index - 2] == " ":
            return LintResult(False, self.id, self.message,
                              line_index, char_index=index - 1)
        if line[index + 3] == " ":
            return LintResult(False, self.id, self.message,
                              line_index, char_index=index + 1)
        return LINT_SUCCESS


class SignalAssignmentLinter(MultilineLinter):

    def __init__(self):
        sub_linters = [G001TabCharacter(),
                       G002TrailingWhitespace(),
                       G003WhitespaceBeforeSemiColon(),
                       G004IncorrectIndentation(),
                       S001SignalAssignmentSpaces()]
        super().__init__("", "", sub_linters=sub_linters)

    # Valid multiline signal assignments
    # * first <=
    #     second;
    # * first <= second(third,
    #                   fourth);
    # * first <= second(
    #              third,
    #              fourth
    #            );
    # * first <= second.third
    #                  .fourth;
    # * first <= second(third downto
    #                   fourth);

    # All of these reduce to a normal form when lines are concatenated and a space
    # added (sometimes):
    # * first <= second;
    # * first <= second(third, fourth);
    # * first <= second(third, fourth);
    # * first <= second.third.fourth;
    # * first <= second(third downto fourth);

    # Don't add a space when:
    # * Next lines first non-whitespace character is `.` or `)`.

    def lint(self, lines, line_index, **kwargs):
        # Simple single line
        if len(lines) == 1:
            return self.sub_lint(lines[0], line_index, **kwargs)
        # Simple multiline
        if len(lines) == 2 and lines[0][-1] == "=":
            first_indent_len = indent_length(lines[0])
            second_indent_len = indent_length(lines[1])
            if second_indent_len != first_indent_len + SPACES_PER_INDENT:
                return LintResult(False, "G004", "Incorrect indentation level",
                                  line_index=line_index + 1,
                                  char_index=second_indent_len - 1)
            return self.sub_lint(f"{lines[0]} {lines[1]}[second_indent_len:]",
                                 line_index, **kwargs)
        # Multiline simple parameter list
        # # Check indent.
        # # Concatenate and full check, then parse out the result to turn back
        # # to an index that makes sense.
        if lines[0][-1] == ",":
        # Multiline complex parameter list
        # # Check indents (do last line first, and use as reference).
        # # Concatenate and full check, then parse out the result to turn back
        # # to an index that makes sense.
        if lines[0][-1] == "(":
        # Multiline record expansion
        # # Check indents (do last line first, and use as reference).
        # # Concatenate and full check, then parse out the result to turn back
        # # to an index that makes sense.
        if "." in lines[0] and ";" not in lines[0]:
        # Multiline range
        # # Check indents (do last line first, and use as reference).
        # # Concatenate and full check, then parse out the result to turn back
        # # to an index that makes sense.
        if lines[0].count("(") != lines[0].count(")"):


def main(args):
    linter_one_indent = SignalAssignmentLinter()
    print(linter_one_indent.lint("  sig <= other_sig;", 1,
                                 expected_indent_level=1))  # Pass
    print(linter_one_indent.lint("  sig <= other_sig;\t", 2,
                                 expected_indent_level=1))  # G001
    print(linter_one_indent.lint("  sig <= other_sig; ", 3,
                                 expected_indent_level=1))  # G002
    print(linter_one_indent.lint("  sig <= other_sig ;", 4,
                                 expected_indent_level=1))  # G004
    print(linter_one_indent.lint("    sig <= other_sig;", 5,
                                 expected_indent_level=1))  # G005
    print(linter_one_indent.lint("sig <= other_sig;", 6,
                                 expected_indent_level=1))  # G005
    print(linter_one_indent.lint("  sig <=other_sig;", 7,
                                 expected_indent_level=1))  # S001
    print(linter_one_indent.lint("  sig<= other_sig;", 8,
                                 expected_indent_level=1))  # S001
    print(linter_one_indent.lint("  sig <=  other_sig;", 9,
                                 expected_indent_level=1))  # S001
    print(linter_one_indent.lint("  sig  <= other_sig;", 10,
                                 expected_indent_level=1))  # S001


if __name__ == "__main__":
    main(sys.argv)
