#!/usr/bin/env python3


from utilities import indent


class Entity:

    class Generic:

        def __init__(self, name, ptype, value=None, last=False):
            self.name = name
            self.ptype = ptype
            self.value = value
            self.last = last

        def to_string(self, n=0):
            last = "" if self.last else ";"
            value = f" := {self.value}" if self.value is not None else ""
            return f"{indent(n)}{self.name} : {self.ptype}{value}{last}"

    class Generics:

        def __init__(self, generics):
            self.generics = []
            for generic in generics:
                if isinstance(generic, tuple):
                    if len(generic) == 2:
                        self.generics.append(Entity.Generic(generic[0],
                                                            generic[1]))
                    else:
                        self.generics.append(Entity.Generic(generic[0],
                                                            generic[1],
                                                            generic[2]))
                else:
                    self.generics.append(generic)
            self.generics[-1].last = True

        def __len__(self):
            return len(self.generics)

        def __iter__(self):
            return iter(self.generics)

        def to_string(self, n=0):
            ret = ""
            for generic in self.generics:
                ret += generic.to_string(n) + "\n"
            return ret

    class Port:

        def __init__(self, name, direction, ptype, last=False):
            self.name = name
            self.direction = direction
            self.ptype = ptype
            self.last = last

        def to_string(self, n=0):
            last = "" if self.last else ";"
            return (f"{indent(n)}{self.name} : "
                    f"{self.direction} {self.ptype}{last}")

    class Ports:

        def __init__(self, ports):
            self.ports = []
            for port in ports:
                if isinstance(port, tuple):
                    self.ports.append(Entity.Port(port[0],
                                                  port[1],
                                                  port[2]))
                else:
                    self.ports.append(port)
            self.ports[-1].last = True

        def __len__(self):
            return len(self.ports)

        def __iter__(self):
            return iter(self.ports)

        def to_string(self, n=0):
            ret = ""
            for port in self.ports:
                ret += port.to_string(n) + "\n"
            return ret

    def __init__(self, name, generics, ports):
        self.name = name
        if isinstance(generics, list):
            self.generics = Entity.Generics(generics)
        else:
            self.generics = generics
        if isinstance(ports, list):
            self.ports = Entity.Ports(ports)
        else:
            self.ports = ports

    def to_string(self, n=0):
        ret = (f"{indent(n)}entity {self.name} is\n"
               + self.format_generics(n + 2)
               + self.format_ports(n + 2) +
               f"{indent(n)}end entity {self.name};")
        return ret

    def format_generics(self, n=0):
        if len(self.generics) != 0:
            return (f"{indent(n)}generic (\n"
                    + self.generics.to_string(n=n + 2) +
                    f"{indent(n)});\n")
        else:
            return ""

    def format_ports(self, n=0):
        return (f"{indent(n)}port (\n"
                + self.ports.to_string(n=n + 2) +
                f"{indent(n)});\n")
